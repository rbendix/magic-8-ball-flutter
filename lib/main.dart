import 'dart:math';

import 'package:flutter/material.dart';
import 'package:shake_event/shake_event.dart';

void main() => runApp(AskMeAnything());

class AskMeAnything extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.blueAccent,
        appBar: AppBar(
          backgroundColor: Colors.blue.shade900,
          title: Text(
            'Ask Me Anything',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25,
            ),
          ),
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Magic8Ball(),
          ),
        ),
      ),
    );
  }
}

class Magic8Ball extends StatefulWidget {
  @override
  _Magic8BallState createState() => _Magic8BallState();
}

class _Magic8BallState extends State<Magic8Ball> with ShakeHandler {

  int imageIndex = 1;

  @override
  void dispose() {
    resetShakeListeners();
    super.dispose();
  }

  @override
  shakeEventListener() {
    randomizeAnswer();
    return super.shakeEventListener();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.startListeningShake(20);
    return Center(
        child: Image.asset('images/ball$imageIndex.png'),
    );
  }

  void randomizeAnswer() {
    setState(() {
      imageIndex = Random().nextInt(5) + 1;
    });
  }
}

